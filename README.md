Scripts for downloading and generating meterological data from which to run GOTM_BFM - see https://wiki.exeter.ac.uk/display/HalloranResearchMain/Creating+new+meteorological+data+forcing+file

This scripts were supplied by Luz in CEFAS and have only been lightly edited
