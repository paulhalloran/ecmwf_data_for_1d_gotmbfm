#!/usr/bin/env python
import sys
from ecmwfapi import ECMWFDataServer
# year
yy = sys.argv[1]
# output file
filename = sys.argv[2]

server = ECMWFDataServer()
server.retrieve({ 
	'class'   : "ei",		
	'dataset' : "era20c",
#	'dataset' : "era40",
	'date'    :  str(yy) + "0101/to/"+ str(yy) + "1231",
	'step'    : "0",
	'levtype' : "sfc",	
	'time'    : "00/06/12/18",
	'type'    : "an",
#	'type'   : "fc",
	'param'   : "151.128/164.128/165.128/166.128/167.128/168.128",
# note the parameter codes for variables can be found here http://apps.ecmwf.int/codes/grib/param-db/
# for example, type in 2 dewpoint and you find 2 metre dewpoint temperature is parameter 168 (matching the last item in teh param list above)
	'grid'    : "0.75/0.75",
# edit this
        'area'    : "65.0/0.0/60.0/5.0",
# Max lat / min lon / min lat / max lon
	'stream'  : "oper",
	'format'  : "netcdf",
	'target'  : "./" + filename 
	})
