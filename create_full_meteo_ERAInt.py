import sys
import netCDF4
import matplotlib.pyplot as plt
import numpy as np
import datetime


yyini=1900
yyend=1902

#Rhodes
#plon=28.6
#plat=35.75

#Patagonia
#plon=360-73
#plat=-42

#Barents
#plon=40
#plat=76.7
#Celtic Deep
#plon=-6.562
#plat= 51.138
#SeanGasField
#plon=4.02
#plat=54.41
#Candyfloss
#plon=-8.6
#plat=49.4
#BenthicA
#plon=-6.1338
#plat=51.2113
#BenthicG
#plon=-6.5811
#plat=51.0726
#Benthic H
#plon=-7.0357
#plat=50.5222
#Benthic I
#plon=-7.1053
#plat=50.5759
#Candyfloss
#plon=-8.6
#plat=49.4
#SmartBuoyCelticDeep2
#plon=-6.562
#plat=51.138
#L4
#plon=-4.2166666
#plat=50.25
#OysterGrounds
#plon=4.02
#plat=54.41
#WestMed
#plon=8.10
#plat=43.20
#John Aldridge request February_2018
plon=2.0
plat=62.0
yy=np.arange(yyini,yyend)

torig = datetime.datetime(1900,1,1,0,0,0)

for y in yy:
	namefile = './ERA-int-' + str(y) + '-ShelfSeas.nc'
	print namefile
	nc = netCDF4.Dataset(namefile)
	if y == yyini:
		lon=nc.variables['longitude'][:]
		lat=nc.variables['latitude'][:]
		time=nc.variables['time'][:]
		lon=np.array(lon)
		lat=np.array(lat)
		auxLon, auxLat = np.meshgrid(lon,lat)
		a = np.where((abs(auxLon-plon)==np.min(abs(auxLon-plon))) & (abs(auxLat-plat)==np.min(abs(auxLat-plat))))
		#print "BE CAREFUL, CORRECTION APPLIED ONLY FOR PATAGONIA"
		#c=range(a[0]-1,a[0]+1)
		#b=range(a[1]-1,a[1]+1)
		print a
		print float(auxLon[a])
		print float(auxLat[a])
		Eratemp=[]
		temp = (np.squeeze(nc.variables['t2m'][:,a[0],a[1]]))
		#temp = np.mean(np.mean(nc.variables['t2m'][:,c,b],axis=1),axis=1)
		print temp.shape
		print time.shape
		for t in temp:
			Eratemp.append(t)
		Erau10=[]
		u10 = (np.squeeze(nc.variables['u10'][:,a[0],a[1]]))
		#u10 = np.mean(np.mean(nc.variables['u10'][:,c,b],axis=1),axis=1)
		for t in u10:
			Erau10.append(t)
		Erav10=[]
		v10 = (np.squeeze(nc.variables['v10'][:,a[0],a[1]]))
		#v10 = np.mean(np.mean(nc.variables['v10'][:,c,b],axis=1),axis=1)
		for t in v10:
			Erav10.append(t)
		Eramslp=[]
		mslp = (np.squeeze(nc.variables['msl'][:,a[0],a[1]]))
		#mslp = np.mean(np.mean(nc.variables['msl'][:,c,b],axis=1),axis=1)
		for t in mslp:
			Eramslp.append(t)
		Eradew=[]
		dew = (np.squeeze(nc.variables['d2m'][:,a[0],a[1]]))
		#dew = np.mean(np.mean(nc.variables['d2m'][:,c,b],axis=1),axis=1)
		for t in dew:
			Eradew.append(t)
		Eratcc=[]
		tcc = (np.squeeze(nc.variables['tcc'][:,a[0],a[1]]))
		#tcc = np.mean(np.mean(nc.variables['tcc'][:,c,b],axis=1),axis=1)
		for t in tcc:
			Eratcc.append(t)
		datefin=[]
		for t in time:
			datefin.append(torig+datetime.timedelta(float(t)/24))
		nc.close()
	else:
		time=nc.variables['time'][:]
		for t in time:
			datefin.append(torig+datetime.timedelta(float(t)/24))
		temp =(np.squeeze(nc.variables['t2m'][:,a[0],a[1]]))
		#temp =np.mean(np.mean(nc.variables['t2m'][:,c,b],axis=1),axis=1)
		for t in temp:
			Eratemp.append(t)
		u10 = (np.squeeze(nc.variables['u10'][:,a[0],a[1]]))
		#u10 = np.mean(np.mean(nc.variables['u10'][:,c,b],axis=1),axis=1)
		for t in u10:
			Erau10.append(t)
		v10 = (np.squeeze(nc.variables['v10'][:,a[0],a[1]]))
		#v10 = np.mean(np.mean(nc.variables['v10'][:,c,b],axis=1),axis=1)
		for t in v10:
			Erav10.append(t)
		mslp = (np.squeeze(nc.variables['msl'][:,a[0],a[1]]))
		#mslp = np.mean(np.mean(nc.variables['msl'][:,c,b],axis=1),axis=1)
		for t in mslp:
			Eramslp.append(t)
		dew = (np.squeeze(nc.variables['d2m'][:,a[0],a[1]]))
		#dew = np.mean(np.mean(nc.variables['d2m'][:,c,b],axis=1),axis=1)
		for t in dew:
			Eradew.append(t)
		tcc = (np.squeeze(nc.variables['tcc'][:,a[0],a[1]]))
		#tcc = np.mean(np.mean(nc.variables['tcc'][:,c,b],axis=1),axis=1)
		for t in tcc:
			Eratcc.append(t)
		nc.close()

datefin=np.array(datefin)
Eratemp=np.array(Eratemp)-273.15
Erau10=np.array(Erau10)
Erav10=np.array(Erav10)
Eramslp=np.array(Eramslp)/100
Eradew=np.array(Eradew)-273.15
Eratcc=np.array(Eratcc)

#10m u winds, 10m v winds, mean sea level pressure, 2m temperature, 2 metre dewpoint temperature, cloud_area_fraction
DATA = np.column_stack ((datefin, np.around(Erau10,decimals=2), np.around(Erav10,decimals=2), np.around(Eramslp,decimals=2), np.around(Eratemp,decimals=2), np.around(Eradew,decimals=2), np.around(Eratcc,decimals=2)))

f = np.savetxt('ERAInt_lon_49.4N_8.6W_meteo.dat',DATA,delimiter="  ",fmt="%-s")
