#!/bin/bash -f


#===========================================================================
# Bash script to extract temperature and salinity data from the
# global reanalysis model ORA-S3
#==========================================================================

#=========================================
# User defined data
#=========================================

Iniyear=1900
numfinaldate=1902

#numfinaldate=19590301


# End of user defined data
#=========================================



# Date initialization in order to download and treat the files

finaldate=$Iniyear
COUNTER=1;
while [ $finaldate -lt $numfinaldate ]
do
        echo $COUNTER
        namefile="$var".2m.gauss."$finaldate".nc
        outfile="ERA-int-"$finaldate"-ShelfSeas.nc"
#	outfile="ERA40-"$finaldate"-Barents.nc"
        echo $outfile 
        echo "Extracting ERA file"
        ipython direct_extract_ecmwf_batch.py $finaldate $outfile
        finaldate=$(($finaldate + 1))
        COUNTER=$(($COUNTER + 1))
done


